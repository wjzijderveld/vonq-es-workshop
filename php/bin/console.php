#!/usr/bin/env php
<?php
require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Application;

$app = new Application('VONQ Eventsourcing Workshop', '0.0.1');
$app->run();
