# PHP Specifics for event sourcing workshop

**NOTE** This README assumes for all commands (unless otherwise specified) that you are in the `php` directory.

## Testing

We'll be using phpspec for all unit tests and behat for the domain tests.


### PHPSpec

Running the tests:

```
vendor/bin/phpspec run
```

### Behat

```
vendor/bin/behat
```
